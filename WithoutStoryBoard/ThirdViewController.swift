//
//  ThirdViewController.swift
//  WithoutStoryBoard
//
//  Created by Rohit Marwaha on 11/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
    var setButton: UIButton!
    var name: UITextField!
    var rollNumber: UITextField!
    
    var rootViewController: RootViewController!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        
        name = UITextField()
        name.frame = CGRect(x: 100, y: 200, width: 300, height: 50)
        name.textColor = .white
        name.backgroundColor = .lightGray
        self.view.addSubview(name)
        
        
        
        rollNumber = UITextField()
        rollNumber.frame = CGRect(x: 100, y: 270, width: 300, height: 50)
        rollNumber.textColor = .white
        rollNumber.backgroundColor = .lightGray
        self.view.addSubview(rollNumber)
        
        
        
        
        setButton = UIButton()
        setButton.frame = CGRect(x: 150, y: 500, width: 150, height: 100)
        setButton.setTitle("Set Data", for: .normal)
        setButton.backgroundColor = .darkGray
        setButton.addTarget(self, action: #selector(returnData), for: UIControl.Event.touchUpInside)
        self.view.addSubview(setButton)

    }
    

    override func viewWillAppear(_ animated: Bool) {
        //
    }
    
    @objc func returnData(){
       
        if let name = name.text , let roll = rollNumber.text {
            rootViewController.secondSaveData(name: name, roll: roll)
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }

}
