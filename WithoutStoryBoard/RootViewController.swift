//
//  RootViewController.swift
//  WithoutStoryBoard
//
//  Created by Rohit Marwaha on 11/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    
    var updateButton: UIButton!
    var secondUpdateButton: UIButton!
    var updateData: UILabel!
    var secondUpdateData: UILabel!
    var savedName: String!
    var savedRoll: String!
    
    var secondSavedName: String!
    var secondSavedRoll: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        updateData = UILabel()
        updateData.frame = CGRect(x: 100, y: 100, width: 150, height: 100)
        updateData.text = "Updated"
        updateData.backgroundColor = .lightGray
        updateData.textColor = .white
        self.view.addSubview(updateData)
        
        
        secondUpdateData = UILabel()
        secondUpdateData.frame = CGRect(x: 100, y: 250, width: 150, height: 100)
        secondUpdateData.text = "Updated"
        secondUpdateData.backgroundColor = .lightGray
        secondUpdateData.textColor = .white
        self.view.addSubview(secondUpdateData)
        
        updateButton = UIButton(type: .custom)
        updateButton.frame = CGRect(x: 150, y: 500, width: 150, height: 100)
        updateButton.setTitle("Update  Button", for: .normal)
        updateButton.backgroundColor = .darkGray
        updateButton.addTarget(self, action: #selector(updateValue), for: UIControl.Event.touchUpInside)
        self.view.addSubview(updateButton)
        
        secondUpdateButton = UIButton(type: .custom)
        secondUpdateButton.frame = CGRect(x: 150, y: 650, width: 150, height: 100)
        secondUpdateButton.setTitle(" Second Update  Button", for: .normal)
        secondUpdateButton.backgroundColor = .darkGray
        secondUpdateButton.addTarget(self, action: #selector(secondUpdateValue), for: UIControl.Event.touchUpInside)
        self.view.addSubview(secondUpdateButton)
        
        
       
    }
    
    func saveData(name: String , roll: String) {
        
        self.savedName = name
        self.savedRoll = roll
    }
    
    func secondSaveData(name: String , roll: String){
        self.secondSavedName = name
        self.secondSavedRoll = roll
    }
    
@objc func updateValue(){
     print("button pressed")
//    
//    secondViewController.dataToPass = updateData.text
//    
    
    
    let secondViewController = SecondViewController()
    secondViewController.rootViewController = self
    self.present(secondViewController, animated: true, completion: nil)
    
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
        if let name = self.savedName  , let roll = self.savedRoll{
             updateData.text = name + " " + roll
        }
        if let name = self.secondSavedName , let roll = self.secondSavedRoll{
            secondUpdateData.text = name + " " + roll
        }
       
    }
    
    @objc func secondUpdateValue(){
        print("button presssed ")
        
        let thirdViewController = ThirdViewController()
        thirdViewController.rootViewController = self
        self.present(thirdViewController , animated: true , completion: nil)
    }
}
